package Data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectDatabase 
{
    
     private static Connection con;
     private static final String DB_URL = "jdbc:mysql://localhost:3306/quanlytaichinhcanhan";
     private static final String SV_NAME= "root";
     private static final String PASSWORD = "76072";

     public ConnectDatabase()
     {}
     
     
     public static Connection getConnection()
     {
          try
          {
              //DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
//               Class.forName("com.mysql.cj.jdbc.Driver");
               Class.forName("com.mysql.jdbc.Driver");
//               Connection con = DriverManager.getConnection(getConnectionUrl());
               con = DriverManager.getConnection(DB_URL,SV_NAME,PASSWORD); 
               if(con != null){
               System.out.println("ket noi thanh cong");
            }
          }
          catch(ClassNotFoundException | SQLException ex)
          {
              System.out.println(ex.getMessage());
              System.out.println("not data");
          }
          return con;
      }
     

     public static void closeConnection()
     {
          try
          {
               if(con!=null)
                    con.close();
               con=null;
          }
          catch(SQLException e)
          {
               e.printStackTrace();
          }
     }
}
